# Achievements_lib Docs

# Table of Contents
* [1. Achievements](#1-achievements)
  * [1.1 Registering an achievement](#11-registering-an-achievement)
  * [1.2 Awarding and removing achievements](#12-awarding-and-removing-achievements)
  * [1.3 Getters](#13-getters)
  * [1.4 Utils](#14-utils)
  * [1.5 GUI](#15-gui)
* [2. About the author(s)](#2-about-the-authors)

## 1. Achievements
Achievements_lib is a library that handles achievements. It provides a system
for awarding achievements and viewing them, as well as a HUD notification on
award.

### 1.1 Registering an achievement

`achvmt_lib.register_mod(mod_name, def)`: registers a mod that will contain achievements.
`def` is a table containing the following fields:
* `name`: (string) the name of the mod. Contrary to `mod_name`, this is not the technical name and it can be translated
* `icon`: (string) the icon representing the mod

`achvmt_lib.register_achievement(technical_name, achievement_def)`: registers an achievement. `technical_name` is a string that must follow the syntax `mod_name:whatever` (name must be unique). `achievement_def` is a table with the following fields:
* `title`: (string) the title of the achievement
* `description`: (string) the description of the achievement
* `image`: (string) the image of the achievement. Should be 26x26
* `tier`: (string) the tier of the string. Can be either `"Bronze"`, `"Silver"` or `"Gold"`
* `hidden`: (boolean) whether to hide the description until unlocked. `false` by default
* `secret`: (boolean) whether to completely hide the achievement (the title will be "Secret Achievement" and there will be a placeholder image until unlocked). `false` by default
* `on_award`: (function(p_name)): function run when awarded
* `on_unaward`: (function(p_name)): function run when unawarded

Example:

```lua
  {
    title = "Ultimate Bounciness",
    description = "Bounce 50 times",
    image = "mymod_ultimate_bounciness.png",
    tier = "Gold"
  }
  ```

After registration, when an achievement definition is returned, it will also contain the indexes `mod` and `name` for easy reference.

### 1.2 Awarding and removing achievements

* `achvmt_lib.award(p_name, ach_name)`: awards an achievement to `p_name`. If the player is not online, then they will receive a hud popup when they next log on. Returns `true` on success and a string communicating whether the call succeeded
* `achvmt_lib.unaward(p_name, ach_name)`: removes an achievement from `p_name`. Returns `true` on success and a string communicating whether the call succeeded
* `achvmt_lib.unaward_all(p_name)`: same as `unaward` but for every achievement


### 1.3 Getters

Some getters take an optional `filter` parameter. `filter` is a table to filter the output and it's structured as such:
```lua
  {
    mod = "My mod", -- modname
    tier = "Bronze", -- tier name
  }
```

* `achvmt_lib.get_achievements(<filter>)`: returns a table of achievements, with names as key and def as value (`{ach_name1 = def1, ach_name2 = def2}`)
* `achvmt_lib.get_achvmt_by_name(ach_name)`: returns the definition table of `ach_name`
* `achvmt_lib.get_amount(<filter>)`: returns the amount of achievements. Currently `filter` only supports `mod`
* `achvmt_lib.get_player_achievements(p_name, <filter>)`: returns a table of achievements unlocked by `p_name`, with the achievement name as value (`{"ach_name1", "ach_name2"})
* `achvmt_lib.get_latest_unlocked(p_name, amount)`: returns a table with the latest `amount` achievements unlocked by `p_name`, starting from the newest to the oldest
* `achvmt_lib.get_mods()`: returns a table with registered mod names as a value
* `achvmt_lib.get_mod_info(mod)`: returns a table containing the information provided when the mod was registered. `nil` if it doesn't exist


### 1.4 Utils

* `achvmt_lib.has_achievement(p_name, ach_name)`: returns whether the player has the specified achievement
* `achvmt_lib.exists(ach_name)`: returns whether `ach_name` exists


### 1.5 GUI

* `achvmt_lib.hud_show(p_name, achievement_def)`: shows a HUD popup to `p_name` when they are awarded an achievement. Override this function to change the default behaviour
* `achvmt_lib.gui_show(p_name, to_p_name)` shows `to_p_name` a menu with a scrollable list of `p_name` achievements. If `to_p_name` is not specified, it'll default to `p_name`. Override this function to implement custom behavior.


## 2. About the author(s)
I'm Zughy (Marco), a professional Italian pixel artist who fights for free software and digital ethics. If this library spared you a lot of time and you want to support me somehow, please consider donating on [Liberapay](https://liberapay.com/Zughy/). I also want to thank MisterE for the initial help
