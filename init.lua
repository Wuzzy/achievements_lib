local version = "0.5.0-dev"
local modpath = minetest.get_modpath("achievements_lib")
local srcpath = modpath .. "/src"

achvmt_lib = {}

if not minetest.get_modpath("lib_chatcmdbuilder") then
    dofile(modpath .. "/libs/chatcmdbuilder.lua")
end

dofile(modpath .. "/SETTINGS.lua")

dofile(srcpath .. "/api.lua")
dofile(srcpath .. "/gui.lua")
dofile(srcpath .. "/hud.lua")
dofile(srcpath .. "/items.lua")
dofile(srcpath .. "/commands.lua")
dofile(srcpath .. "/player_manager.lua")
dofile(srcpath .. "/privs.lua")

if achvmt_lib.DEBUG_MODE then
    dofile(srcpath .. "/tests.lua")
end

minetest.log("action", "[ACHIEVEMENTS_LIB] Mod initialised, running version " .. version)
