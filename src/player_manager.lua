minetest.register_on_joinplayer(function(player, last_login)
  local p_name = player:get_player_name()

  achvmt_lib.load_achievements(p_name)
  achvmt_lib.hud_create(p_name)
end)