local S = minetest.get_translator("achievements_lib")

local player_scroll_indexes = {}
local players_viewing = {} -- whose acheivements each player is viewing
local player_tabs = {}
local hide_achieved = {}        -- KEY: p_name, VALUE: boolean
local first_visible_tab = {} -- first visible tab for each player

----------------------------------------------
-----------Predeclare local funcs-------------
----------------------------------------------

local function make_scrollbaroptions_for_scroll_container() return end
local function get_tabs() return end
local function sort() return end

-- Makes and returns the achivement list that goes in the scroll container. Also
-- returns the formspec-length of the list for the scrollbar options. @p_name is
-- the player who has the achievments, @to_p_name is the player to whom the
-- achievements are shown, defaults to p_name. @filter is a filter table as
-- detailed in DOCS.md, and @hide states whether to hide unlocked achievements
local function get_achievement_list_fs(p_name, to_p_name, filter, hide) return end

-- returns table of available mod tabnames
local function get_available_tabs() return end


----------------------------------------------
----------------    API     ------------------
----------------------------------------------

-- GUI (FORMSPEC)
function achvmt_lib.gui_show(p_name, to_p_name)
  to_p_name = to_p_name or p_name
  players_viewing[to_p_name] = p_name -- keep track of which achievements fs to_p_name is viewing
  player_tabs[to_p_name] = player_tabs[to_p_name] or "ALL" -- the tab name for each player

  local visible_l = 5.875
  local tot_all_ach = achvmt_lib.get_amount()
  local hide_ach = hide_achieved[to_p_name]

  -- filter by mod if in a mod tab
  local filter = {}
  if player_tabs[to_p_name] ~= "ALL" then
    filter.mod = player_tabs[to_p_name]
    tot_all_ach = achvmt_lib.get_amount({mod = filter.mod})
  end

  -- more counters
  local tot_achieved_ach = #achvmt_lib.get_player_achievements(p_name, filter)
  local tot_unachieved_ach = tot_all_ach - tot_achieved_ach
  local ach_list, total_l, tot_p_ach = get_achievement_list_fs(p_name, to_p_name, filter, hide_ach)
  local reward_bg = "achievements_lib_fs_bg.png"
  -- TODO: custom bg when all achievements have been unlocked
  --local reward_bg = tot_achieved_ach == tot_all_ach and "achievements_lib_fs_bg_ach.png" or "achievements_lib_fs_bg.png"

  -- get images for filter icons ... the seleced one gets a green highlight
  local unach_only = hide_ach and "achievements_lib_fs_filter_on.png" or "achievements_lib_fs_filter_off.png"

  local scrollbar = ""
  player_scroll_indexes[to_p_name] = player_scroll_indexes[to_p_name] or 0

  if total_l >= visible_l then
    scrollbar = make_scrollbaroptions_for_scroll_container(visible_l, total_l, .1, "hide") ..
                "scrollbar[12.8125,2.625;.125," .. visible_l .. ";vertical;ACH_SCROLL;" .. player_scroll_indexes[to_p_name] .. "]"
  end

  local fs = {
    "formspec_version[6]",
    "size[15,10]",
    "padding[.01,.01]",
    "no_prepend[]",
    "bgcolor[;neither;]",
    "background[0,0;15,10;" .. reward_bg .. "]",
    "hypertext[2,1;11,1;title;<global size=22 halign=center valign=middle><style color=#cfc6b8><b>" .. S("@1's achievements", p_name) .. "</b>]",
    "hypertext[2,2;11,0.6;amount;<global size=18 halign=center valign=middle><style color=#cfc6b8><b>" .. tot_achieved_ach .. "/" .. tot_all_ach .. "</b>]",
    "style[P_NAME;border=false;font=bold;font_size=*.9]",
    "style_type[image_button;border=false]",
    "image_button[0.93,2.97;0.8,0.8;" .. unach_only .. ";HASNT;]",
    "tooltip[0.93,2.97;0.8,0.8;" .. S("Filter by Unachieved") .. ";#7a444a;#f4cca1]",
    "scroll_container[2.14,2.625;10.8125," .. visible_l .. ";ACH_SCROLL;vertical;.1]",
    ach_list or "",
    "scroll_container_end[]",
    scrollbar,
    get_tabs(p_name, to_p_name)
  }

  minetest.show_formspec(to_p_name, "achvmt_lib_disp", table.concat(fs, ""))
end

----------------------------------------------
-------------  Functions defined -------------
----------------------------------------------

-- This Function MIT by Rubenwardy
--- Creates a scrollbaroptions for a scroll_container
--
-- @param visible_l the length of the scroll_container and scrollbar
-- @param total_l length of the scrollable area
-- @param scroll_factor as passed to scroll_container
function make_scrollbaroptions_for_scroll_container(visible_l, total_l, scroll_factor, arrows)
  assert(total_l >= visible_l)
  arrows = arrows or "default"
  local thumb_size = (visible_l / total_l) * (total_l - visible_l)
  local max = total_l - visible_l

  return ("scrollbaroptions[min=0;max=%f;thumbsize=%f;arrows=%s]"):format(max / scroll_factor, thumb_size / scroll_factor, arrows)
end



-- filter is for mod, view is A,U,or AU and filters per-player by whether
-- they have the achievement or not
function get_achievement_list_fs(p_name, to_p_name, filter, hide)
  local all_achievements = achvmt_lib.get_achievements(filter)

  -- order player's achievements first, then unachieved ones. Totally secret,
  -- locked achievements go last.

  local ach_list = {}
  local secret_ach_list = {}
  local tot_p_ach = 0

  if hide then
    for ach_name, ach in pairs(all_achievements) do
      -- tests for player having an achievement, use this instead of the
      -- built-in function to reduce calculation
      if not achvmt_lib.has_achievement(to_p_name, ach_name) then
        -- store secret achievements to add in last
        if ach.secret then
            table.insert(secret_ach_list, ach)
        else
            table.insert(ach_list, ach)
        end
        tot_p_ach = tot_p_ach + 1
      end
    end

  else
    for ach_name, ach in pairs(all_achievements) do
      table.insert(ach_list, ach)
      tot_p_ach = tot_p_ach + 1
    end
  end

  -- tier > mod > name
  table.sort(ach_list, function(a, b) return sort(a,b) end)

  -- TODO: put secret achievements at the end of every tier
  -- add in any secret achievements last
  for i, ach in ipairs(secret_ach_list) do table.insert(ach_list, ach) end

  local fs = {}
  local count = 0
  local list_height_multiplier = 1.5
  local total_height = 0

  if not next(ach_list) then
    fs = {
      "image[0,0;10.75," .. list_height_multiplier .. ";achievements_lib_fs_unachlist_bg.png]",
      "style[INFO;border=false;textcolor=#dff6f5;font=italic;font_size=*1]",
      "button[0,0;10.75," .. list_height_multiplier .. ";INFO;" .. S("No achievements ... yet") .. "]"
    }

    total_height = list_height_multiplier

  else
    -- build the ach list fs from the ach_list
    for _, ach in ipairs(ach_list) do
      local ach_name = ach.name
      local has_ach = achvmt_lib.has_achievement(to_p_name, ach_name)
      local mod = achvmt_lib.get_mod_info(ach.mod)

      -- alternate bg colors in list
      local bg_img = "achievements_lib_fs_bg_" .. string.lower(ach.tier)

      local y_pos = count * list_height_multiplier
      local mod_icon = mod.icon or "blank.png"
      local mod_text = mod.name or ""
      local ach_image = ach.image
      local ach_title = ach.title
      local ach_hidden_text = ach.hint or "???" .. " " -- need space so minetest doesnt cut off the italics text
      local ach_description = (ach.hidden and not has_ach) and arc_hidden_text or ach.description
      local grey_out

      if has_ach then
        grey_out = ""
      else
        if hide_achieved[p_name] then
          grey_out = "box[0," .. (y_pos - .02) .. ";10.75," .. (list_height_multiplier + 0.02) .. ";#22222277]"
        else
          grey_out = "box[0," .. (y_pos - .02) .. ";10.75," .. (list_height_multiplier + 0.02) .. ";#2222228d]"
        end
      end

      if ach.secret then
        ach_title = "???"
        ach_image = "achievements_lib_secret.png"
        ach_hidden_text = "???" .. " " -- TODO: capisci qualcosa su differenza hidden_text e description
      end

      total_height = total_height + list_height_multiplier

      local ach_fs = {
        "image[0," .. y_pos .. ";10.75," .. list_height_multiplier .. ";" .. bg_img .. ".png]",
        "image[0.2," .. (y_pos + 0.1) .. ";1.3,1.3;" .. ach_image .. "]",
        "image[9.8," .. (y_pos + 0.4) .. ";0.65,0.65;" .. mod_icon .. "]",
        "tooltip[9.8," .. (y_pos + 0.4) .. ";0.65,0.65;" .. mod_text .. ";#7a444a;#f4cca1]",
        "style_type[label;border=false;textcolor=#472d3c;font=bold;font_size=*1.4]",
        "label[1.8," .. (y_pos + 0.45) .. ";" .. ach_title .. "]",
        "style_type[label;border=false;textcolor=#472d3c;font=italic;font_size=*0.9]",
        "label[1.8," .. (y_pos + 1) .. ";" .. ach_description .. "]",
        grey_out
      }

      for _, v in pairs(ach_fs) do
        table.insert(fs, v)
      end

      count = count + 1
    end
  end

  return table.concat(fs, ""), total_height, tot_p_ach
end



function get_tabs(p_name, to_p_name)
  local available_tabs = {}
  local max_visible_tabs = 8
  local available_tabs = get_available_tabs()
  first_visible_tab[to_p_name] = first_visible_tab[to_p_name] or 1

  local fs = {}
  local visible_tabs = {}

  for i = first_visible_tab[to_p_name], first_visible_tab[to_p_name] + max_visible_tabs do
    if available_tabs[i] then
      table.insert(visible_tabs, available_tabs[i])
    end
  end

  for i, tabname in ipairs(visible_tabs) do -- tabname is modname or "ALL"
    local x = 2.8 + (i - 1) * 1.05
    local bg = player_tabs[to_p_name] == tabname and "achievements_lib_tab_bg_selected.png" or "achievements_lib_tab_bg.png"
    local unlocked_ach, tot_ach, texture, tooltip

    if tabname == "ALL" then
      unlocked_ach = #achvmt_lib.get_player_achievements(p_name)
      tot_ach = achvmt_lib.get_amount()
      texture = "achievements_lib_item.png"
      tooltip = S("All Achievements")
    else
      local mod = achvmt_lib.get_mod_info(tabname)

      unlocked_ach = #achvmt_lib.get_player_achievements(p_name, {mod = tabname})
      tot_ach = achvmt_lib.get_amount({mod = tabname})
      texture = mod.icon
      tooltip = S("Achievements from") .. " " .. mod.name
    end

    tooltip = tooltip .. " (" .. unlocked_ach .. "/" .. tot_ach .. ")"

    local tab_fs = {
      "style_type[image_button;border=false]",
      "image[" .. x .. ",8.58;0.97,0.97;" .. bg .. "]",
      "image_button[" .. x + .133 .. ",8.65;0.73,0.73;" .. texture .. ";" .. tabname .. ";]",
      "tooltip[" .. x .. ",8.58;0.97,0.97;" .. tooltip .. ";#7a444a;#f4cca1]"
    }

    for _, v in pairs(tab_fs) do
      table.insert(fs, v)
    end
  end

  -- show arrows
  if (#available_tabs > max_visible_tabs) and (first_visible_tab[to_p_name] ~= 1) then
    table.insert(fs, "image_button[" .. 32 / 16 .. "," .. 138 / 16 .. ";" .. 8 / 16 .. "," .. 14 / 16 .. ";achievements_lib_arrow_left.png;TAB_LEFT;]")
  end

  if (#available_tabs > max_visible_tabs) and (first_visible_tab[to_p_name] ~= #available_tabs - max_visible_tabs) then
    table.insert(fs, "image_button[" .. 201 / 16 .. "," .. 138 / 16 .. ";" .. 8 / 16 .. "," .. 14 / 16 .. ";achievements_lib_arrow_right.png;TAB_RIGHT;]")
  end

  return table.concat(fs, "")
end



function get_available_tabs()
  local available_tabs = {}
  table.insert(available_tabs, "ALL")
  for _, mod in pairs(achvmt_lib.get_mods()) do
    table.insert(available_tabs, mod)
  end

  return available_tabs
end





----------------------------------------------
-------------      Callback      -------------
----------------------------------------------

minetest.register_on_player_receive_fields(function(player, formname, fields)
  if formname ~= "achvmt_lib_disp" then return end

  local p_name = player:get_player_name()
  local available_tabs = get_available_tabs()

  if fields.HASNT then
    hide_achieved[p_name] = not hide_achieved[p_name]
    achvmt_lib.gui_show(players_viewing[p_name], p_name)
  end

  -- mod tabs
  for _, tabname in ipairs(available_tabs) do
    if fields[tabname] then
      player_tabs[p_name] = tabname
      achvmt_lib.gui_show(players_viewing[p_name], p_name)
    end
  end

  -- move mod tab arrows; buttons wont be shown if they are not supposed to
  -- be, so we dont have to check.
  if fields.TAB_LEFT then
    first_visible_tab[p_name] = first_visible_tab[p_name] - 1
    achvmt_lib.gui_show(players_viewing[p_name], p_name)
  end

  if fields.TAB_RIGHT then
    first_visible_tab[p_name] = first_visible_tab[p_name] + 1
    achvmt_lib.gui_show(players_viewing[p_name], p_name)
  end
end)



function sort(a, b)
  local a_tier = a.tier
  local b_tier = b.tier

  if a_tier == b_tier then
    local a_name = a.mod .. a.name  -- to consider translations as well
    local b_name = b.mod .. b.name
    local names = {a_name, b_name}

    table.sort(names)
    return names[1] ~= a_name
  elseif a.tier == "Bronze" or b.tier == "Gold" then
    return true
  else
    return false
  end
end