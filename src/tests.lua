achvmt_lib.register_mod("achievements_lib", {
  name = "Achievements Lib",
  icon = "logo.png"
})

achvmt_lib.register_mod("arena_lib", {
  name = "Arena Lib",
  icon = "arenalib_customise_bgm.png"
})

achvmt_lib.register_mod("balloon_bop", {
  name = "Balloon Bop",
  icon = "balloon_bop_hudballoon.png"
})

achvmt_lib.register_mod("bucket", {
  name = "Bucket",
  icon = "bucket.png"
})

achvmt_lib.register_mod("default", {
  name = "Default",
  icon = "default_stone.png"
})

achvmt_lib.register_mod("fireflies", {
  name = "Fireflies",
  icon = "fireflies_bottle.png"
})

achvmt_lib.register_mod("flowers", {
  name = "Flowers",
  icon = "flowers_waterlily.png"
})

achvmt_lib.register_mod("worldedit", {
  name = "World Edit",
  icon = "worldedit_wand.png"
})



achvmt_lib.register_achievement("achievements_lib:achvmt_lib_tests_ach_1", {
  title = "Ultimate Bounciness",
  description = "Bounce on a trampoline 5 times.",
  image = "default_tree.png",
  tier = "Bronze",
  hidden = false
})

achvmt_lib.register_achievement("achievements_lib:achvmt_lib_tests_ach_2", {
  title = "Penultimate Bounciness",
  description = "Bounce on a trampoline 20 times.",
  image = "default_tree_top.png",
  tier = "Silver",
  hidden = false
})

achvmt_lib.register_achievement("achievements_lib:achvmt_lib_tests_ach_3", {
  title = "UltiPenultimate Bounciness",
  description = "Bounce on a trampoline 200 times.",
  image = "default_stone.png",
  tier = "Gold",
  hidden = false
})

achvmt_lib.register_achievement("achievements_lib:achvmt_lib_tests_ach_4", {
  title = "More Bounciness",
  description = "Bounce on a trampoline 400 times.",
  image = "default_cobble.png",
  tier = "Gold",
  hidden = false
})

achvmt_lib.register_achievement("achievements_lib:achvmt_lib_tests_ach_5", {
  title = "Even More Bounciness",
  description = "Bounce on a trampoline 500 times.",
  image = "default_stone_block.png",
  tier = "Gold",
  hidden = false
})

achvmt_lib.register_achievement("default:stick", {
  title = "Stick",
  description = "Get a stick",
  image = "default_stick.png",
  tier = "Bronze",
  hidden = false
})

achvmt_lib.register_achievement("fireflies:firefly", {
  title = "firefly",
  description = "catch a firefly",
  mod_icon = "fireflies_bottle.png",
  mod_title = "Fireflies",
  image = "fireflies_bugnet.png",
  tier = "Bronze",
  hidden = true
})

achvmt_lib.register_achievement("balloon_bop:bop", {
  title = "bop",
  description = "Bop",
  image = "balloon_bop_hudballoon.png",
  tier = "Bronze",
  hidden = false
})

achvmt_lib.register_achievement("flowers:waterlilly", {
  title = "Floating Point",
  description = "Get a waterlilly",
  image = "flowers_waterlily.png",
  tier = "Bronze",
  hidden = false
})

achvmt_lib.register_achievement("bucket:bucket", {
  title = "Bucket",
  description = "Get a bucket",
  image = "bucket.png",
  tier = "Bronze",
  secret = true
})

achvmt_lib.register_achievement("arena_lib:music", {
  title = "Music",
  description = "set music",
  mod_icon = "arenalib_customise_bgm.png",
  mod_title = "Arena Lib",
  image = "arenalib_customise_bgm.png",
  tier = "Bronze",
  secret = false
})

achvmt_lib.register_achievement("worldedit:wand", {
  title = "Wand",
  description = "Use wand",
  image = "worldedit_wand.png",
  tier = "Bronze",
  secret = false
})

-- show another player's achievements on rightclick
minetest.register_on_rightclickplayer(function(player, clicker)
  local p_name = player:get_player_name()
  local to_p_name = clicker:get_player_name()
  achvmt_lib.gui_show(p_name, to_p_name)
end)
